<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

$domain = $_SERVER['SERVER_NAME'];

if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  
// Otherwise use the below settings (on live server)
} else {
 
  // Live Server Database Settings
  define( 'DB_NAME',     'bsac2014');
  define( 'DB_USER',     'bsac2014');
  define( 'DB_PASSWORD', '7ada0e5593fa730dbe038880cfcf70cf' );
  define( 'DB_HOST',     'localhost'  );
  
  // Overwrites the database to save keep edeting the DB
  define('WP_HOME','http://bsac.org.uk');
  define('WP_SITEURL','http://bsac.org.uk');
  
  // Turn Debug off on live server
  define('WP_DEBUG', false);
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vi }(_b!1hzka:F~HBa-jp5o/Uesu3HC}-VP7# ]JC1DqPWAn#OT[CAeK(4R`,4>');
define('SECURE_AUTH_KEY',  'ycXLvOIw-QkZjm/UNk]Y1PuxjF>`122%vmSI($HaAMR1Kg@7%M)T`I VO3V6?ftK');
define('LOGGED_IN_KEY',    '-^r6/6VYQ.$&~u`$iDWQqqhj/G26yBI4nWGj.9hUM/QV^-U6iLH$&yw#UVh4,yJY');
define('NONCE_KEY',        '/#EJT$SR|;K#=q{TJiz7n@UI>$gVdXRa*Ypoj+hc.{$lukWRtT26^Bg[2!GIiH<v');
define('AUTH_SALT',        ' H7PIOhXruT?j9}oGS6Vk/1X=E2H:]V~&f]{(dUJ~2/9z5}?>@bh?K% 0(;V ZX4');
define('SECURE_AUTH_SALT', 'p_w:r5qYg}{l_]a}IjUug_`+hbPo9]?vXENC8  D;7<]/D3dj}32]9Kv)I?%9RhT');
define('LOGGED_IN_SALT',   'pp^^rMQ8.u-B[e^.a5^ky%G:t`}>KBGY&T H)cl[Sk73sk(+j I)/c6T_@x~I%s&');
define('NONCE_SALT',       '<?R!uACn0/^I!9J3K[VJ=0a]MpRm-lpW!T,V-{p0q@zF8z#f.=8&B,:[Nc>!dMdq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
