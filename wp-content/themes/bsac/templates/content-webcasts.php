<?php while (have_posts()) : the_post(); ?>

	<div <?php post_class('webcast') ?> id="post-<?php the_ID(); ?>">

		<?php the_post_thumbnail('pmix_webcasts_thumbnail', array('class' => 'webcasts-thumb')); ?>

		<strong id="post-<?php the_ID(); ?>"><?php the_title(); ?></strong>

		<div class="entry">

			<?php the_content(); ?>

		</div>

	</div>

<?php endwhile; ?>