<?php while (have_posts()) : the_post(); ?>
	<div class="top-section">
		<div class="span10">
			<?php wp_nav_menu( 
		      array('menu' => 'funding menu',
		            'items_wrap' => '<ul>%3$s</ul>' 
		      ));
		    ?>
		</div>
		<div class="span2 bsac-page-logo" style="text-align:right;">
			<img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-colour.png">
		</div>
	</div>
	<div class="main-content">
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>