<?php while (have_posts()) : the_post(); ?>
			

<div class="row">
	<div class="span8">
		<img class="eccmid-title" src="<?php bloginfo('template_directory'); ?>/assets/img/eccmid-page-title.jpg" alt="Invitation to Evening Reception" />
	</div>
	<div class="span4">
		<img class="eccmid-float-right" src="<?php bloginfo('template_directory'); ?>/assets/img/eccmid-logo.png" alt="ECCMID logo" />
	</div>
</div>	

<div class="row">
	<div class="span6">
		<?php the_content(); ?>
	</div>
	<div class="span6">
		<img class="eccmid-float-right" src="<?php bloginfo('template_directory'); ?>/assets/img/eccmid-gallery.jpg" alt="Karaya images" />
	</div>

</div>
	
<?php endwhile; ?>