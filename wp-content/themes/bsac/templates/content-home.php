
<!-- START REVOLUTION SLIDER 4.3.8 fullscreen mode -->

<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:#E9E9E9;padding:0px;min-height:850px !important;">
  <div id="rev_slider_1_1" class="rev_slider fullscreenbanner" style="display:none;">
    <ul>  <!-- SLIDE  -->
      <li data-transition="fade" data-slotamount="0" data-masterspeed="300" >
        <!-- MAIN IMAGE -->
        <img src="assets/home-bkgnd.jpg" alt="home-bkgnd" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption black tp-fade tp-resizeme"
          data-x="0"
          data-y="0" 
          data-speed="300"
          data-start="500"
          data-easing="Power3.easeInOut"
          data-splitin="none"
          data-splitout="none"
          data-elementdelay="0.1"
          data-endelementdelay="0.1"
          data-endspeed="300"
          style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap; width:100%;">
                  <div class="left-boxes">
                    <div id="scroller">
                      <?php wp_nav_menu( 
                        array('menu' => 'Standards and Service Development',
                              'items_wrap' => '<div class="menu-title-holder"><span>Standards and Service Development</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>
                      <?php wp_nav_menu( 
                        array('menu' => 'Professional/Public Engagement',
                              'items_wrap' => '<div class="menu-title-holder"><span>Professional/Public Engagement</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>
                      <?php wp_nav_menu( 
                        array('menu' => 'Publications',
                              'items_wrap' => '<div class="menu-title-holder"><span>Publications</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>

                      <?php wp_nav_menu( 
                        array('menu' => 'Surveillance',
                              'items_wrap' => '<div class="menu-title-holder"><span>Surveillance</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>
                      <?php wp_nav_menu( 
                        array('menu' => 'BSAC International',
                              'items_wrap' => '<div class="menu-title-holder"><span>BSAC International</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>
                      <?php wp_nav_menu( 
                        array('menu' => 'Working With BSAC',
                              'items_wrap' => '<div class="menu-title-holder"><span>Working With BSAC</span></div>
                                               <div class="menu-items-holder"><ul>%3$s</ul></div>' 
                        ));
                      ?>
                    </div>
                  </div>
                  <div class="home-circle">
                    <?php while (have_posts()) : the_post(); ?>
                      <div class="home-circle-logo"><img src="<?php the_field('bsac_homepage_logo'); ?>" /></div>
                      <div class="home-circle-text"><?php the_content() ?></div>
                    <?php endwhile; ?>
                    <ul class="nav nav-tabs videoTabs" id="myTab">
                      <li id="videos-tab" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Featured Videos</a>
                        <ul class="dropdown-menu">
                          <?php
                            $args = array(
                              'post_type' =>'homepage-videos',
                              'homepage-video-type' => 'add-to-homepage',
                              'order' => 'ASC',
                              'posts_per_page' => -1
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                          ?>
                          <?php if( get_field('video_url') ):?>
                            <li>
                              <?php 
                                $videoURL = get_field('video_url');
                                $videoTitle = get_the_title();
                                $youtubeVideo = "[su_lightbox type=iframe src={$videoURL}]{$videoTitle}[/su_lightbox]";
                                echo do_shortcode( $youtubeVideo );
                              ?>
                            </li>
                          <?php endif; ?>
                            
                          <?php endwhile; ?>
                        </ul>
                      </li>
                    </ul>
                  </div>

                  

                  <div class="right-boxes">
                      <ul class="nav nav-tabs" id="myTab">
                        <li id="combined-tab" class="active"><a class="combined" href="#combined" data-toggle="tab">Combined</a></li>
                        <li id="news-tab"><a class="dropdown-toggle newsfeed" data-toggle="tab" href="#news">News</a></li>
                        <li id="twitter-tab"><a class="dropdown-toggle twitterfeed" data-toggle="tab" href="#twitter">Twitter</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane fade in active" id="combined">
                          <?php
                            $args = array(
                              'post_type'=>'post',
                              'posts_per_page'=>'8',
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                            
                            $cats = get_the_category($recent["ID"]);
                          ?>
                            <div class="homerighttweetnews">
                              <div class="<?php foreach($cats as $cat) { echo $cat->name." "; } ?>">
                                <div class="homerighttweetnews-link">
                                  <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                  </a>
                                </div>
                              </div>
                            </div>

                          <?php endwhile; ?>
                        </div>
                        <div class="tab-pane fade" id="news">
                          <?php
                            $args = array(
                              'post_type'=>'post',
                              'cat'=>9,
                              'posts_per_page'=>'8',
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                            
                            $cats = get_the_category($recent["ID"]);
                          ?>
                            <div class="homerighttweetnews">
                              <div class="News">
                                <div class="homerighttweetnews-link">
                                  <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                  </a>
                                </div>
                              </div>
                            </div>

                          <?php endwhile; ?>
                        </div>
                        <div class="tab-pane fade" id="twitter">
                          <?php
                            $args = array(
                              'post_type'=>'post',
                              'cat'=>37,
                              'posts_per_page'=>'8',
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                            
                            $cats = get_the_category($recent["ID"]);
                          ?>
                            <div class="homerighttweetnews">
                              <div class="Tweets">
                                <div class="homerighttweetnews-link">
                                  <a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                  </a>
                                </div>
                              </div>
                            </div>

                          <?php endwhile; ?>
                        </div>
                      </div>
                  </div>
        </div>
        <div id="content-indicator"><a href="#nextsection" class="more-content-indicator"></a></div>
      </li>
    </ul>
    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
  </div>
</div>      
      <script type="text/javascript">

        var tpj=jQuery;       
        tpj.noConflict();       
        var revapi1;
        
        tpj(document).ready(function() {
                
        if(tpj('#rev_slider_1_1').revolution == undefined)
          revslider_showDoubleJqueryError('#rev_slider_1_1');
        else
           revapi1 = tpj('#rev_slider_1_1').show().revolution(
          {
            dottedOverlay:"none",
            delay:9000,
            startwidth:960,
            startheight:350,
            hideThumbs:200,
            
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:1,
            
            navigationType:"none",
            navigationArrows:"none",
            navigationStyle:"round",
            
            touchenabled:"on",
            onHoverStop:"off",
            
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
                        
            keyboardNavigation:"off",
            
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,

            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,

            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
                
            shadow:0,
            fullWidth:"off",
            fullScreen:"on",

            spinner:"spinner0",
            
            stopLoop:"on",
            stopAfterLoops:0,
            stopAtSlide:1,

            
            shuffle:"off",
            
                        
            forceFullWidth:"on",            
            fullScreenAlignForce:"on",            
            minFullScreenHeight:"",           
            hideTimerBar:"on",            
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,            
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            videoJsPath:"/wp-content/plugins/revslider/rs-plugin/videojs/",
            fullScreenOffsetContainer: ".banner"  
          });
        
        }); //ready
        
      </script>
      
      <!-- END REVOLUTION SLIDER -->

  	<div class="home-arc-section">
  		<div class="wrap container" role="document">
        <div class="content row">
          <div class="<?php echo roots_main_class(); ?>" role="main">
            <div class="span7" style="margin:0;">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/arc01.png" />
            </div>
            <div class="span2">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/arc-visit.png" />
            </div>
            <div class="span3">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/arc-logo.png" />
            </div>
          </div>
        </div>
      </div>
  	</div>
