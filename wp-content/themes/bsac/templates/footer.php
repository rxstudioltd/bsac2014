<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> - Admin</p>
	<p>By using this site, you agree we can set and use cookies. For more details of these cookies and how to disable them, see our cookie policy.</p>
  </div>
</footer>

<?php wp_footer(); ?>




