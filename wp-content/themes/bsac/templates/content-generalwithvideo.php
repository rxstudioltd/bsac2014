<?php while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
<?php endwhile; ?>

<script>
    function loadVideo(videoUrl)
    {
        var left = (screen.width/2)-(1280/2);
        var top = (screen.height/2)-(720/2);
        var newVideoUrl = "http://bsac.org.uk/PlayVideoPage.html?url=" + videoUrl;
        newVideoUrl = encodeURI(newVideoUrl);
        launchApplication(newVideoUrl,'window2',top,left);
    }
    function launchApplication(l_url, l_windowName, top, left)
    {
      var isChrome = /chrome/.test(navigator.userAgent.toLowerCase());
      if ( typeof launchApplication.winrefs == 'undefined' )
      {
        launchApplication.winrefs = {};
      }
      if (isChrome && launchApplication.winrefs.hasOwnProperty(l_windowName))
      {
        launchApplication.winrefs[l_windowName].close();
      }
      if (!launchApplication.winrefs.hasOwnProperty(l_windowName) || launchApplication.winrefs[l_windowName].closed || isChrome)
      {
        var l_params = '_self, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width='+1350+', height='+800+', top='+top+', left='+left;
        launchApplication.winrefs[l_windowName] = window.open(l_url, l_windowName, l_params);
      } else {      
        setTimeout(function(){launchApplication.winrefs[l_windowName].focus();},200);
        launchApplication.winrefs[l_windowName].location.href=l_url;
      }
    }   
</script>