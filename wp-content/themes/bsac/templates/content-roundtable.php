<?php while (have_posts()) : the_post(); ?>
			
				<div class="roundtable-title">
					<div class="roundtable-title-holder">
						<h1><?php the_field('roundtable-large-pull-text'); ?></h1>
					</div>
					<div class="roundtable-img"><img src="<?php bloginfo('template_directory'); ?>/assets/img/roundtable-top-img.jpg" alt="UK Antimicrobial Resistance Roundtable Meetings" /></div>
				</div>	

				<div class="roundtable-intro-text">
					<?php the_content(); ?>
				</div>
				
				<div class="row">
					<div class="span4">
						<div class="roundtable-one-top">
							<span class="small-text">ROUNDTABLE ONE</span>
							<h2><?php the_field('roundtable-one-title'); ?></h2>
						</div>
						<div class="roundtable-one-bottom venue-info">
							<?php the_field('roundtable-one-date-venue'); ?>
						</div>
						<a href="<?php the_field('roundtable-one-download-agenda'); ?>">
							<div class="roundtable-one-bottom">
								<span class="small-text">DOWNLOAD AGENDA</span>
							</div>
						</a>
						<a href="<?php the_field('roundtable-one-register'); ?>">
							<div class="roundtable-one-bottom">
								<span class="small-text">REGISTER HERE</span>
							</div>
						</a>
					</div>
					<div class="span4">
						<div class="roundtable-two-top">
							<span class="small-text">ROUNDTABLE TWO</span>
							<h2><?php the_field('roundtable-two-title'); ?></h2>
						</div>
						<div class="roundtable-one-bottom venue-info">
							<?php the_field('roundtable-two-date-venue'); ?>
						</div>
						<a href="<?php the_field('roundtable-two-download-agenda'); ?>">
							<div class="roundtable-two-bottom">
								<span class="small-text">DOWNLOAD AGENDA</span>
							</div>
						</a>
						<a href="<?php the_field('roundtable-two-register'); ?>">
							<div class="roundtable-two-bottom">
								<span class="small-text">REGISTER HERE</span>
							</div>
						</a>
					</div>
					<div class="span4">
						<div class="roundtable-three-top">
							<span class="small-text">ROUNDTABLE THREE</span>
							<h2><?php the_field('roundtable-three-title'); ?></h2>
						</div>
						<div class="roundtable-one-bottom venue-info">
							<?php the_field('roundtable-three-date-venue'); ?>
						</div>
						<a href="<?php the_field('roundtable-three-download-agenda'); ?>">
							<div class="roundtable-three-bottom">
								<span class="small-text">DOWNLOAD AGENDA</span>
							</div>
						</a>
						<a href="<?php the_field('roundtable-three-register'); ?>">
							<div class="roundtable-three-bottom">
								<span class="small-text">REGISTER HERE</span>
							</div>
						</a>
					</div>
				</div>

				<div class="roundtable-lower-logo">
				In association with <img src="<?php bloginfo('template_directory'); ?>/assets/img/4allofuslogo.jpg" alt="4 all of us logo" />
			</div>

<?php endwhile; ?>