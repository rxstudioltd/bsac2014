<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/assets/img/site-icon.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/assets/img/site-icon.png">
  <link rel="apple-touch-icon-precomposed" href="<?php bloginfo('template_directory'); ?>/assets/img/site-icon.png">
  <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon.ico">
  
  <?php wp_head(); ?>

  <!--[if lt IE 9]>
    <style>
      .rev_slider_wrapper {
        height:850px !important;
      }
      .ieeightfallbackimage {
        width:100%;
        height:100% !important;
      }
    </style>
  <![endif]-->

  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

  <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.simplyscroll.js"></script>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/jquery.simplyscroll.css" media="all" type="text/css">
  <script type="text/javascript">
  (function($) {
    $(function() {
      $("#scroller").simplyScroll({orientation:'vertical',customClass:'vert'});
    });
  })(jQuery);
  </script>

  <script>
  	var j = jQuery.noConflict();
      j(function() {
        j('#rev_slider_1_1_wrapper ul li #content-indicator a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
              || location.hostname == this.hostname) {

            var target = j(this.hash);
            target = target.length ? target : j('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              j('html,body').animate({
                scrollTop: target.offset().top-0
              }, 1000);
              return false;
            }
          }
        });
      });
    </script>

</head>
