<a href="<?php echo home_url(); ?>/past-meetings/"><h6>To view Past Meetings click here</h6></a>
<p>&nbsp;</p>
<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; ?>

<?php
$post_type = 'meetings';
$tax = 'meetings-type';
$tax_terms = get_terms($tax, array('orderby' => 'id', 'order' => 'ASC'));
if ($tax_terms) {
    foreach ($tax_terms as $tax_term) {
        $args = array(
            'post_type' => $post_type,
            "$tax" => $tax_term->slug,
            'orderby' => 'title',
            'order' => 'ASC',
            ); // END $args
        $my_query = null;
        $my_query = new WP_Query($args);
        if ($my_query->have_posts()) {
        	echo '<div class="row"><div class="span10"><h2 style="color:#08233e;">' . $tax_term->name . '</h2></div>';
            while ($my_query->have_posts()) : $my_query->the_post();
            ?>

            <div class="span3">
	            <div class="meeting-item">
					<?php if( has_post_thumbnail() ) { ?>
						<?php if( get_field('external_url') ) {?>
							<a href="<?php the_field('external_url'); ?>" target="_blank">
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>">
						<?php } ?>
							<div class="meeting-item-thumb"><?php the_post_thumbnail(); ?></div></a>
					<?php } else { ?>
						<a href="<?php the_permalink(); ?>"><div class="meeting-item-thumb-holding"><img src="<?php bloginfo('template_directory'); ?>/assets/img/meeting-holding.png"></div></a>
					<?php } ?>
					<div class="meeting-item-text"><?php the_title(); ?></div>
					<a class="meeting-item-button" href="<?php the_permalink(); ?>">VIEW MEETING INFORMATION &amp; RESOURCES</a>
				</div>
			</div>

          	<?php endwhile; } ?>
        </div>
        <p>&nbsp;</p>
       <?php wp_reset_query();
    } // END foreach $tax_terms
} // END if $tax_terms
?>



