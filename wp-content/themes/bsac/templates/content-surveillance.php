<?php while (have_posts()) : the_post(); ?>

	<div class="top-section">
		<div class="span10">
			<?php wp_nav_menu( 
		      array('menu' => 'surveillance menu',
		            'items_wrap' => '<ul>%3$s</ul>' 
		      ));
		    ?>
		</div>
		<div class="span2 bsac-page-logo" style="text-align:right;">
			<img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-colour.png">
		</div>
	</div>
	<div class="main-content">
		<?php the_content(); ?>
	</div>
	<p>&nbsp;</p>
	<?php
		$args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'orderby' => 'date', 'order' => 'DESC');
		$attachments = get_posts($args);
		
		if($attachments){
			// lalapoo!
			// sorry lords of the programming universe…
			// it has to be done
			$list_items = array();
			foreach($attachments as $attachment){
				$url = wp_get_attachment_url($attachment->ID);
				$bits = explode(".",$url);
				$count = count($bits);
				
				array_pop($bits);
				
				$url = implode(".", $bits);
				
				if(!stristr(get_the_content(), $url)){
					$list_items[] = do_shortcode('[su_button url="'.wp_get_attachment_url($attachment->ID).'" target="blank" style="flat" background="#08233e" size="4" radius="0" icon="icon: file-o"]'.apply_filters('the_title', $attachment->post_title).'[/su_button]');
				}
			}
			
			if(count($list_items)){
			?>
				<div id="dldocs">
				
					<h2>Download Documents</h2>
					
					<ul>
						<?php									
							foreach($list_items as $li){
								echo '<li>'.$li.'</li>';
							}
						?>
					</ul>
					
				</div>
			<?php
			}
		}
	?>

<?php endwhile; ?>