<header class="banner navbar navbar-static-top" role="banner">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="<?php echo home_url(); ?>/"><img src="<?php bloginfo('template_url'); ?>/assets/img/white-bsac-logo.png" /></a>
      <nav class="nav-main nav-collapse collapse" role="navigation">
        <?php wp_nav_menu( 
          array('menu' => 'initiatives', 'menu_class' => 'nav', 
          ));
        ?>
      </nav>
      
      <a class="join-button" href="<?php bloginfo('template_url'); ?>/membership-details/">
        JOIN BSAC
      </a>
    </div>
  </div>
</header>
