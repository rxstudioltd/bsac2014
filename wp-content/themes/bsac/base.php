<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 7]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->

  <?php
    do_action('get_header');
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    } else {
      get_template_part('templates/header');
    }
  ?>

  <?php if (is_front_page()) { ?>
          <?php include roots_template_path(); ?>
        <?php }
        else { ?>

          <div class="page-header-inner">
            <div class="container">
              <h1>
                <?php echo roots_title(); ?>
              </h1>
            </div>
          </div>

          <div class="wrap container" role="document">
            <div class="content row">
              <div class="main span12" role="main">
                <?php include roots_template_path(); ?>
              </div><!-- /.main -->
            </div><!-- /.content -->
          </div><!-- /.wrap -->
  <?php } ?>

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
