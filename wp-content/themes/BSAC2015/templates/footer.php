<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
	<p><small>By using this site, you agree we can set and use cookies. For more details of these cookies and how to disable them, see our cookie policy.</small></p>
  	<p><small>ADMIN LOGIN</small></p>
  </div>
</footer>
<?php wp_footer(); ?>
