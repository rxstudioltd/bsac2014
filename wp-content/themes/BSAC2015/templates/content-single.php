<div class="internal-page">
  <?php while (have_posts()) : the_post(); ?>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  <?php endwhile; ?>
</div>
<script>
    /*jQuery(document).ready(function () {
        jQuery("#EditDialog").dialog({
            autoOpen: false,
            title: "",
            beforeClose: function() {
                $('.videoClass').attr('src', '');
            },
            modal: true,
            cache: false,
            width: 1024,
            height: 720
        });
    });*/

    function loadVideo(videoUrl)
    {
    var left = (screen.width/2)-(1280/2);
    var top = (screen.height/2)-(720/2);
    var newVideoUrl = "http://bsac.org.uk/PlayVideoPage.html?url=" + videoUrl;
    newVideoUrl = encodeURI(newVideoUrl);
    myWindow = window.open(newVideoUrl, 'window2', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width='+1350+', height='+800+', top='+top+', left='+left);
    }
</script>
