<div class="internal-page">
	<?php while (have_posts()) : the_post(); ?>

		<div class="top-section">
			<?php wp_nav_menu( 
		      array('menu' => 'funding menu',
		            'items_wrap' => '<ul>%3$s</ul>' 
		      ));
		    ?>
		</div>

		<h1><?php the_title(); ?></h1>
	 	<?php the_content(); ?>
	<?php endwhile; ?>
</div>
