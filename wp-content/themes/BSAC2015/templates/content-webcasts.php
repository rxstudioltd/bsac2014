<div class="internal-page">
	<?php while (have_posts()) : the_post(); ?>
		<?php the_post_thumbnail('pmix_webcasts_thumbnail', array('class' => 'webcasts-thumb')); ?>
		<h1><?php the_title(); ?></h1>
	 	<?php the_content(); ?>
	<?php endwhile; ?>
</div>