<div class="internal-page">
	<h1>Latest News</h1>
	<p>&nbsp;</p>
	<?php
          $args = array(
            'post_type' =>'news',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
          );
          $query = new WP_Query($args);
          while ($query->have_posts()) : $query->the_post();
        ?>
          <div class="news-item">
            <div class="date-box">
              <h2><?php the_time('j'); ?></h2>
              <?php the_time('M'); ?>
            </div>
            <div class="news-item-text">
              <a class="news-heading" href="<?php the_permalink(); ?>"><h6><?php the_title(); ?></h6></a>
              <?php the_excerpt(); ?>
            </div>
          </div>
    <?php endwhile; ?>
</div>
