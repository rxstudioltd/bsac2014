<div class="internal-page">
	<?php while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
	 	<?php the_content(); ?>

	 	<?php
				$args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'orderby' => 'date', 'order' => 'DESC');
				$attachments = get_posts($args);
				
				if($attachments){
					// lalapoo!
					// sorry lords of the programming universe…
					// it has to be done
					$list_items = array();
					foreach($attachments as $attachment){
						$url = wp_get_attachment_url($attachment->ID);
						$bits = explode(".",$url);
						$count = count($bits);
						
						array_pop($bits);
						
						$url = implode(".", $bits);
						
						if(!stristr(get_the_content(), $url)){
							$list_items[] = '<li><a href="'.wp_get_attachment_url($attachment->ID).'" target="_blank">'.apply_filters('the_title', $attachment->post_title).'</a></li>';
						}
					}
					
					if(count($list_items)){
					?>
						<div id="dldocs">
						
							<h2>Download Documents</h2>
							
							<ul>
								<?php									
									foreach($list_items as $li){
										echo $li;
									}
								?>
							</ul>
							
						</div>
					<?php
					}
				}
			?>
	<?php endwhile; ?>
</div>
