<div class="internal-page">
    <?php while (have_posts()) : the_post(); ?>
        <a href="<?php echo home_url(); ?>/latest-meetings/"><h6>To view Latest Meetings click here</h6></a>
        <h2><?php the_title(); ?></h2>
    <?php endwhile; ?>
    
    <?php
    $post_type = 'past-meetings';
    $tax = 'past-meetings-type';
    $tax_terms = get_terms($tax, array('orderby' => 'id', 'order' => 'DESC'));
    if ($tax_terms) {
        foreach ($tax_terms as $tax_term) {
            $args = array(
                'post_type' => $post_type,
                "$tax" => $tax_term->slug,
                'orderby' => 'title',
                'order' => 'ASC',
                ); // END $args
            $my_query = null;
            $my_query = new WP_Query($args);
            if ($my_query->have_posts()) {
                echo '<h2 style="color:#08233e;">' . $tax_term->name . '</h2><ul class="meeting-section">';
                while ($my_query->have_posts()) : $my_query->the_post();
                ?>

                <li class="meeting-entry">
                    <div class="meeting-item">
                        <?php if( has_post_thumbnail() ) { ?>
                            <?php if( get_field('external_url') ) {?>
                                <a href="<?php the_field('external_url'); ?>" target="_blank">
                            <?php } else { ?>
                                <a href="<?php the_permalink(); ?>">
                            <?php } ?>
                                <div class="meeting-item-thumb"><?php the_post_thumbnail(); ?></div></a>
                        <?php } else { ?>
                            <a href="<?php the_permalink(); ?>"><div class="meeting-item-thumb-holding"><img src="<?php bloginfo('template_directory'); ?>/assets/img/meeting-holding.png"></div></a>
                        <?php } ?>
                        <div class="meeting-item-text"><?php the_title(); ?></div>
                        <a class="meeting-item-button" href="<?php the_permalink(); ?>">VIEW MEETING INFORMATION &amp; RESOURCES</a>
                    </div>
                </li>

                <?php endwhile; } ?>
            </ul>
           <?php wp_reset_query();
        } // END foreach $tax_terms
    } // END if $tax_terms
    ?>
</div>







