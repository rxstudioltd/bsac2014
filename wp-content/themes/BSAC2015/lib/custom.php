<?php
/**
 * Custom functions
 */

function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/assets/img/logo-colour.png) !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');



/* Create the editor Custom Post Type ------------------------------------------*/
function create_post_type_editor() 
{
    $labels = array(
        'name' => __( 'Editor Feature' ),
        'singular_name' => __( 'Editor' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New Editor Feature'),
        'edit_item' => __('Edit Editor Feature'),
        'new_item' => __('New Editor Feature'),
        'view_item' => __('View Editor Feature'),
        'search_items' => __('Search Editor Feature'),
        'not_found' =>  __('No Editor Feature found'),
        'not_found_in_trash' => __('No Editor Feature found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'Documents' ), 
        'supports' => array('title','editor','thumbnail','page-attributes','excerpt'),
        'menu_icon' => get_template_directory_uri() . '/assets/img/ui-custom.png'
      ); 
      
      register_post_type(__( 'editor' ),$args);
}

/* Create the Editor Feature Type Taxonomy --------------------------------------------*/
function build_taxonomies_editor(){
    $labels = array(
        'name' => __( 'Editor Feature Type' ),
        'singular_name' => __( 'Editor Feature Type' ),
        'search_items' =>  __( 'Search Editor Feature Types' ),
        'popular_items' => __( 'Popular Editor Feature Types' ),
        'all_items' => __( 'All Editor Feature Types' ),
        'parent_item' => __( 'Parent Editor Feature Type' ),
        'parent_item_colon' => __( 'Parent Editor Feature Type:' ),
        'edit_item' => __( 'Edit Editor Feature Type' ), 
        'update_item' => __( 'Update Editor Feature Type' ),
        'add_new_item' => __( 'Add New Editor Feature Type' ),
        'new_item_name' => __( 'New Editor Feature Type Name' ),
        'separate_items_with_commas' => __( 'Separate Editor Feature types with commas' ),
        'add_or_remove_items' => __( 'Add or remove Editor Feature types' ),
        'choose_from_most_used' => __( 'Choose from the most used Editor Feature types' ),
        'menu_name' => __( 'Editor Feature Types' )
    );
    
    register_taxonomy(
        'editor-feature-type', 
        array( __( 'editor' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'editor-feature-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_editor' );
add_action( 'init', 'build_taxonomies_editor', 0 );



/* Create the News Custom Post Type ------------------------------------------*/
function create_post_type_news() 
{
    $labels = array(
        'name' => __( 'News Feature' ),
        'singular_name' => __( 'News' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New News Feature'),
        'edit_item' => __('Edit News Feature'),
        'new_item' => __('New News Feature'),
        'view_item' => __('View News Feature'),
        'search_items' => __('Search News Feature'),
        'not_found' =>  __('No News Feature found'),
        'not_found_in_trash' => __('No News Feature found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'News' ), 
        'supports' => array('title','editor','thumbnail','page-attributes','excerpt'),
        'menu_icon' => get_template_directory_uri() . '/assets/img/ui-custom.png'
      ); 
      
      register_post_type(__( 'news' ),$args);
}

/* Create the News Feature Type Taxonomy --------------------------------------------*/
function build_taxonomies_news(){
    $labels = array(
        'name' => __( 'News Feature Type' ),
        'singular_name' => __( 'News Feature Type' ),
        'search_items' =>  __( 'Search News Feature Types' ),
        'popular_items' => __( 'Popular News Feature Types' ),
        'all_items' => __( 'All News Feature Types' ),
        'parent_item' => __( 'Parent News Feature Type' ),
        'parent_item_colon' => __( 'Parent News Feature Type:' ),
        'edit_item' => __( 'Edit News Feature Type' ), 
        'update_item' => __( 'Update News Feature Type' ),
        'add_new_item' => __( 'Add New News Feature Type' ),
        'new_item_name' => __( 'New News Feature Type Name' ),
        'separate_items_with_commas' => __( 'Separate News Feature types with commas' ),
        'add_or_remove_items' => __( 'Add or remove News Feature types' ),
        'choose_from_most_used' => __( 'Choose from the most used News Feature types' ),
        'menu_name' => __( 'News Feature Types' )
    );
    
    register_taxonomy(
        'news-feature-type', 
        array( __( 'news' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'news-feature-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_news' );
add_action( 'init', 'build_taxonomies_news', 0 );



/* Create the Meetings Custom Post Type ------------------------------------------*/
function create_post_type_meetings() 
{
    $labels = array(
        'name' => __( 'Meetings' ),
        'singular_name' => __( 'Meeting' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New Meeting'),
        'edit_item' => __('Edit Meeting'),
        'new_item' => __('New Meeting'),
        'view_item' => __('View Meeting'),
        'search_items' => __('Search Meetings'),
        'not_found' =>  __('No Meetings found'),
        'not_found_in_trash' => __('No Meetings found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'Meetings' ), 
        'supports' => array('title','editor','thumbnail','page-attributes','excerpt'),
        'menu_icon' => get_template_directory_uri() . '/assets/img/ui-custom.png'
      ); 
      
      register_post_type(__( 'meetings' ),$args);
}

/* Create the Meeting Type Taxonomy --------------------------------------------*/
function build_taxonomies_meetings(){
    $labels = array(
        'name' => __( 'Meeting Type' ),
        'singular_name' => __( 'Meeting Type' ),
        'search_items' =>  __( 'Search Meeting Types' ),
        'popular_items' => __( 'Popular Meeting Types' ),
        'all_items' => __( 'All Meeting Types' ),
        'parent_item' => __( 'Parent Meeting Type' ),
        'parent_item_colon' => __( 'Parent Meeting Type:' ),
        'edit_item' => __( 'Edit Meeting Type' ), 
        'update_item' => __( 'Update Meeting Type' ),
        'add_new_item' => __( 'Add New Meeting Type' ),
        'new_item_name' => __( 'New Meeting Type Name' ),
        'separate_items_with_commas' => __( 'Separate Meeting types with commas' ),
        'add_or_remove_items' => __( 'Add or remove Meeting types' ),
        'choose_from_most_used' => __( 'Choose from the most used Meeting types' ),
        'menu_name' => __( 'Meeting Types' )
    );
    
    register_taxonomy(
        'meetings-type', 
        array( __( 'meetings' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'meetings-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_meetings' );
add_action( 'init', 'build_taxonomies_meetings', 0 );



add_filter( 'manage_edit-meetings_columns', 'my_edit_meetings_columns' ) ;
/* Create extra columns for Meetings --------------------------------------------*/
function my_edit_meetings_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Meeting Name' ),
        'meetings-type' => __( 'Meeting Type' ),
        'date' => __( 'Date' )
    );

    return $columns;
}
 
add_action( 'manage_meetings_posts_custom_column', 'my_manage_meetings_columns', 10, 2 );

function my_manage_meetings_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'meetings-type' column. */
        case 'meetings-type' :

            /* Get the genres for the post. */
            $terms = get_the_terms( $post_id, 'meetings-type' );

            /* If terms were found. */
            if ( !empty( $terms ) ) {

                $out = array();

                /* Loop through each term, linking to the 'edit posts' page for the specific term. */
                foreach ( $terms as $term ) {
                    $out[] = sprintf( '<a href="%s">%s</a>',
                        esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'meetings-type' => $term->slug ), 'edit.php' ) ),
                        esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'meetings-type', 'display' ) )
                    );
                }

                /* Join the terms, separating them with a comma. */
                echo join( ', ', $out );
            }

            /* If no terms were found, output a default message. */
            else {
                _e( 'No Meetings' );
            }

            break;

        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}



/* Create the Past Meetings Custom Post Type ------------------------------------------*/
function create_post_type_past_meetings() 
{
    $labels = array(
        'name' => __( 'Past Meetings' ),
        'singular_name' => __( 'Past Meeting' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New Past Meeting'),
        'edit_item' => __('Edit Past Meeting'),
        'new_item' => __('New Past Meeting'),
        'view_item' => __('View Past Meeting'),
        'search_items' => __('Search Past Meetings'),
        'not_found' =>  __('No Past Meetings found'),
        'not_found_in_trash' => __('No Past Meetings found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'past-meetings' ), 
        'supports' => array('title','editor','thumbnail','page-attributes','excerpt'),
        'menu_icon' => get_template_directory_uri() . '/assets/img/ui-custom.png'
      ); 
      
      register_post_type(__( 'past-meetings' ),$args);
}

/* Create the Past Meeting Type Taxonomy --------------------------------------------*/
function build_taxonomies_past_meetings(){
    $labels = array(
        'name' => __( 'Past Meeting Type' ),
        'singular_name' => __( 'Past Meeting Type' ),
        'search_items' =>  __( 'Search Past Meeting Types' ),
        'popular_items' => __( 'Popular Past Meeting Types' ),
        'all_items' => __( 'All Past Meeting Types' ),
        'parent_item' => __( 'Parent Past Meeting Type' ),
        'parent_item_colon' => __( 'Parent Past Meeting Type:' ),
        'edit_item' => __( 'Edit Past Meeting Type' ), 
        'update_item' => __( 'Update Past Meeting Type' ),
        'add_new_item' => __( 'Add New Past Meeting Type' ),
        'new_item_name' => __( 'New Past Meeting Type Name' ),
        'separate_items_with_commas' => __( 'Separate Past Meeting types with commas' ),
        'add_or_remove_items' => __( 'Add or remove Past Meeting types' ),
        'choose_from_most_used' => __( 'Choose from the most used Past Meeting types' ),
        'menu_name' => __( 'Past Meeting Types' )
    );
    
    register_taxonomy(
        'past-meetings-type', 
        array( __( 'past-meetings' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'past-meetings-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_past_meetings' );
add_action( 'init', 'build_taxonomies_past_meetings', 0 );

add_filter( 'manage_edit-past-meetings_columns', 'my_edit_past_meetings_columns' ) ;
/* Create extra columns for Past Meetings --------------------------------------------*/
function my_edit_past_meetings_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Meeting Name' ),
        'past-meetings-type' => __( 'Meeting Year' ),
        'date' => __( 'Date' )
    );

    return $columns;
}
 
add_action( 'manage_past-meetings_posts_custom_column', 'my_manage_past_meetings_columns', 10, 2 );

function my_manage_past_meetings_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'past-meetings-type' column. */
        case 'past-meetings-type' :

            /* Get the genres for the post. */
            $terms = get_the_terms( $post_id, 'past-meetings-type' );

            /* If terms were found. */
            if ( !empty( $terms ) ) {

                $out = array();

                /* Loop through each term, linking to the 'edit posts' page for the specific term. */
                foreach ( $terms as $term ) {
                    $out[] = sprintf( '<a href="%s">%s</a>',
                        esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'past-meetings-type' => $term->slug ), 'edit.php' ) ),
                        esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'past-meetings-type', 'display' ) )
                    );
                }

                /* Join the terms, separating them with a comma. */
                echo join( ', ', $out );
            }

            /* If no terms were found, output a default message. */
            else {
                _e( 'No Past Meetings' );
            }

            break;

        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}



/* Create the Homepage Videos Custom Post Type ------------------------------------------*/
function create_post_type_homepage_videos() 
{
    $labels = array(
        'name' => __( 'Homepage Videos' ),
        'singular_name' => __( 'Homepage Video' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New Homepage Video'),
        'edit_item' => __('Edit Homepage Video'),
        'new_item' => __('New Homepage Video'),
        'view_item' => __('View Homepage Videos'),
        'search_items' => __('Search Homepage Videos'),
        'not_found' =>  __('No Homepage Videos found'),
        'not_found_in_trash' => __('No Homepage Videos found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'homepage-videos' ), 
        'supports' => array('title','editor','thumbnail','page-attributes','excerpt'),
        'menu_icon' => get_template_directory_uri() . '/assets/img/ui-custom.png'
      ); 
      
      register_post_type(__( 'homepage-videos' ),$args);
}

/* Create the Homepage Videos Taxonomy --------------------------------------------*/
function build_taxonomies_homepage_videos(){
    $labels = array(
        'name' => __( 'Homepage Video Type' ),
        'singular_name' => __( 'Past Homepage Videos' ),
        'search_items' =>  __( 'Search Homepage Videos' ),
        'popular_items' => __( 'Popular Homepage Videos' ),
        'all_items' => __( 'All Homepage Videos' ),
        'parent_item' => __( 'Parent Homepage Videos' ),
        'parent_item_colon' => __( 'Parent Homepage Videos:' ),
        'edit_item' => __( 'Edit Homepage Videos' ), 
        'update_item' => __( 'Update Homepage Videos' ),
        'add_new_item' => __( 'Add New Homepage Videos' ),
        'new_item_name' => __( 'New Homepage Videos Name' ),
        'separate_items_with_commas' => __( 'Separate Homepage Videos with commas' ),
        'add_or_remove_items' => __( 'Add or remove Homepage Videos' ),
        'choose_from_most_used' => __( 'Choose from the most used Homepage Videos' ),
        'menu_name' => __( 'Homepage Videos Type' )
    );
    
    register_taxonomy(
        'homepage-video-type', 
        array( __( 'homepage-videos' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'homepage-video-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_homepage_videos' );
add_action( 'init', 'build_taxonomies_homepage_videos', 0 );


