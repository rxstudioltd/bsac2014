<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 7]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header-top-navbar');
  ?>

  <div class="side-panel">
    <div class="side-panel-inner">
      <div class="section-twitter">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/side-panel-twitter.png" />
        <?php dynamic_sidebar('sidebar-twitter'); ?>
      </div>
      <div class="section-news">
        <h5 class="section-news-title">LATEST NEWS</h5>
        <?php
          $args = array(
            'post_type' =>'news',
            'posts_per_page' => '8',
            'orderby' => 'date',
            'order' => 'DESC',
          );
          $query = new WP_Query($args);
          while ($query->have_posts()) : $query->the_post();
        ?>
          <div class="news-item">
            <div class="date-box">
              <h2><?php the_time('j'); ?></h2>
              <?php the_time('M'); ?>
            </div>
            <div class="news-item-text">
              <a class="news-heading" href="<?php the_permalink(); ?>"><h6><?php the_title(); ?></h6></a>
              <?php the_excerpt(); ?>
            </div>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>

  <div class="main-content">
    <div class="wrap container" role="document">
      <div class="content row">
        <div class="main span12" role="main">
          <?php include roots_template_path(); ?>
        </div><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php get_template_part('templates/footer'); ?>
  </div><!-- /.main-content -->

</body>
</html>
